#!/usr/bin/php
<?php
require __DIR__ . '/vendor/autoload.php';

use Dotenv\Dotenv;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\GuzzleException;
use splitbrain\phpcli\CLI;
use splitbrain\phpcli\Options;

class Minimal extends CLI
{
    // register options and arguments
    protected function setup(Options $options)
    {
        $options->setHelp('A very minimal script that retrieves your public IP and updates a DNS record on Cloudflare');
        $options->registerOption('version', 'print version', 'v');
        $options->registerArgument('domain', 'DNS record to update');
    }

    // implement your code
    protected function main(Options $options)
    {
        $ip = $this->getPublicIp();
        $args = $options->getArgs();
        $record = reset($args);
        $this->updateCloudflareRecord($record, $ip);

    }

    private function getPublicIp()
    {
        $client = new Client();
        return (string) $client->get('https://ifconfig.me/ip')->getBody();
    }

    private function updateCloudflareRecord($record, $value)
    {
        $dotenv = Dotenv::createImmutable(__DIR__);
        $dotenv->load();
        $dotenv->required(['CF_API_KEY', 'CF_ZONE', 'CF_RECORD'])->notEmpty();

        $client = new Client();
        $apiKey = $_ENV['CF_API_KEY'];
        $zone = $_ENV['CF_ZONE'];
        $recordId = $_ENV['CF_RECORD'];

        $baseUrl = "https://api.cloudflare.com/client/v4/zones/";
        $options = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $apiKey,
            ],
            'json' => [
                'type' => 'A',
                'name' => $record,
                'content' => $value,
                'ttl' => 1,
                'proxied' => true,
            ]
        ];
        try {
            $zones = (string) $client->put($baseUrl . "$zone/dns_records/$recordId", $options)->getBody();
        } catch (BadResponseException $e) {
            var_dump((string)$e->getResponse()->getBody());
            exit;
        }
        echo $zones;
        exit;
    }
}
// execute it
$cli = new Minimal();
$cli->run();
